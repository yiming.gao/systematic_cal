#import sys
import subprocess
import argparse
import os
import time
import numpy as np
import re
#import plotly.express as px

from hyperactive import Hyperactive
from hyperactive.optimizers import RandomAnnealingOptimizer
from hyperactive.optimizers import RepulsingHillClimbingOptimizer
from hyperactive.optimizers import HillClimbingOptimizer
from hyperactive.optimizers import RandomRestartHillClimbingOptimizer
from hyperactive.optimizers import SimulatedAnnealingOptimizer
from hyperactive.optimizers import ParallelTemperingOptimizer
#from hyperactive.dashboards import ProgressBoard


def modify_config_geant4(input_config, output_config, parameters_tochange):
    with open(input_config, "rt") as fin:
        with open(output_config, "wt") as fout:
            for line in fin:
                if "WASA_Calib_X" in line:
                    fout.write(line.replace("WASA_Calib_X 0.0 mm", "WASA_Calib_X "+str(parameters_tochange["WASA_Calib_X"])+" mm"))
                    continue
                if "WASA_Calib_Y" in line:
                    fout.write(line.replace("WASA_Calib_Y 0.0 mm", "WASA_Calib_Y "+str(parameters_tochange["WASA_Calib_Y"])+" mm"))
                    continue
                if "WASA_Calib_Z" in line:
                    fout.write(line.replace("WASA_Calib_Z 0.0 mm", "WASA_Calib_Z "+str(parameters_tochange["WASA_Calib_Z"])+" mm"))
                    continue
                if "WASA_Calib_Rot_X" in line:
                    fout.write(line.replace("WASA_Calib_Rot_X 0.0 degree", "WASA_Calib_Rot_X "+str(parameters_tochange["WASA_Calib_Rot_X"])+" degree"))
                    continue
                if "WASA_Calib_Rot_Y" in line:
                    fout.write(line.replace("WASA_Calib_Rot_Y 0.0 degree", "WASA_Calib_Rot_Y "+str(parameters_tochange["WASA_Calib_Rot_Y"])+" degree"))
                    continue
                if "WASA_Calib_Rot_Z" in line:
                    fout.write(line.replace("WASA_Calib_Rot_Z 0.0 degree", "WASA_Calib_Rot_Z "+str(parameters_tochange["WASA_Calib_Rot_Z"])+" degree"))
                    continue
                if "MFT_Calib_X" in line:
                    fout.write(line.replace("MFT_Calib_X 0.0 mm","MFT_Calib_X "+str(parameters_tochange["MFT_Calib_X"])+" mm"))
                    continue
                if "MFT_Calib_Y" in line:
                    fout.write(line.replace("MFT_Calib_Y 0.0 mm","MFT_Calib_Y "+str(parameters_tochange["MFT_Calib_Y"])+" mm"))
                    continue
                if "MFT_Calib_Z" in line:
                    fout.write(line.replace("MFT_Calib_Z 0.0 mm","MFT_Calib_Z "+str(parameters_tochange["MFT_Calib_Z"])+" mm"))
                    continue
                if "MFT_Calib_Rot_X" in line:
                    fout.write(line.replace("MFT_Calib_Rot_X 0.0 degree","MFT_Calib_Rot_X "+str(parameters_tochange["MFT_Calib_Rot_X"])+" degree"))
                    continue
                if "MFT_Calib_Rot_Y" in line:
                    fout.write(line.replace("MFT_Calib_Rot_Y 0.0 degree","MFT_Calib_Rot_Y "+str(parameters_tochange["MFT_Calib_Rot_Y"])+" degree"))
                    continue
                if "MFT_Calib_Rot_Z" in line:
                    fout.write(line.replace("MFT_Calib_Rot_Z 0.0 degree","MFT_Calib_Rot_Z "+str(parameters_tochange["MFT_Calib_Rot_Z"])+" degree"))
                    continue
                if "ConvertRoot" in line:
                    fout.write(line.replace("ConvertRoot ./geo_file/WASA_Zero_MFT_Zero.root","ConvertRoot ./geo_file/"+parameters_tochange["GeoNameOut"]))
                    continue

                fout.write(line)


def run_command(command):
    try:
        result = subprocess.run(command, shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        # Print the output
        print("Output:")
        print(result.stdout.decode('utf-8'))
        # Print any errors
        if result.stderr:
            print("Errors:")
            print(result.stderr.decode('utf-8'))
    except subprocess.CalledProcessError as e:
        # Handle errors if the command execution fails
        print("Error:", e)

def parse_variance(output):
    # Define a regular expression pattern to match variance values
    pattern = r'Variance ([xyz]): ([-+]?\b(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][-+]?\d+)?\b)'
    #pattern = r'Variance ([xyz]): (\d+\.\d+)'
    # Use re.findall to extract all matches of the pattern from the output
    matches = re.findall(pattern, output)
    var_dict = {}
    for match in matches:
        print("match: ", match)
        var_dict[match[0]] = float(match[1])
    print(var_dict)
    return var_dict


def run_command_parseOutput(command):
    try:
        result = subprocess.run(command, shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        # Print the output
        print("Output:")
        print(result.stdout.decode('utf-8'))

        dict_res = parse_variance(result.stdout.decode('utf-8'))
        tempXvar = float(dict_res["x"])
        tempYvar = float(dict_res["y"])
        tempZvar = float(dict_res["z"])

        # Print any errors
        if result.stderr:
            print("Errors:")
            print(result.stderr.decode('utf-8'))

        return tempXvar, tempYvar, tempZvar

    except subprocess.CalledProcessError as e:
        # Handle errors if the command execution fails
        print("Error:", e)

        return None



def run_geant4(output_config):
    command_changedir="echo \"current dir:\" `pwd` && cd ~/Simu/build && "
    command_geant4=command_changedir+"./G4SolenoidSimple -m ./macro/test.mac -c "+output_config+" testGUI.root"
    print("command G4 :"+command_geant4)
    run_command(command_geant4)

def run_reco(parameters_tochange):
    command_changedir="echo \"current dir:\" `pwd` && cd ~/FullReco && rm -f ./output/"+parameters_tochange["RecoBaseNameOut"]+"_*.root && rm -f ./output/"+parameters_tochange["RecoBaseNameOut"]+"_*_finish && "
    command_reco=command_changedir+"./submitjob-reco.sh "+parameters_tochange["RecoBaseNameOut"]+" geo/"+parameters_tochange["GeoNameOut"]
    print("command Reco :"+command_reco)
    run_command(command_reco)

def run_anaCal(parameters_tochange):
    command_changedir="echo \"current dir:\" `pwd` && cd ~/FullReco/draw && rm -f ./Hist/Hist_"+parameters_tochange["RecoBaseNameOut"]+"_*.root && rm -f ./Hist/Hist_"+parameters_tochange["RecoBaseNameOut"]+"_*_finish && "
    command_anaCal=command_changedir+"./submitjob.sh "+parameters_tochange["RecoBaseNameOut"]
    print("command Ana :"+command_anaCal)
    run_command(command_anaCal)

def run_addHist(parameters_tochange):
    command_changedir="echo \"current dir:\" `pwd` && cd ~/FullReco/draw && rm -f ./Scan2/"+parameters_tochange["RecoBaseNameOut"]+"_720000.root && "
    command_addHist=command_changedir+"hadd ./Scan2/"+parameters_tochange["RecoBaseNameOut"]+"_720000.root ./Hist/Hist_"+parameters_tochange["RecoBaseNameOut"]+"_*.root"
    print("command AddHist :"+command_addHist)
    run_command(command_addHist)

def run_createVar(parameters_tochange):
    command_changedir="echo \"current dir:\" `pwd` && cd ~/FullReco/draw && "
    command_createVar=command_changedir+"root -l -q \"Read.C(\\\""+parameters_tochange["RecoBaseNameOut"]+"_720000\\\")\""
    print("command createVar :"+command_createVar)

    return run_command_parseOutput(command_createVar)


def lossfunction(parameters_tochange,variances_file,out_file,loss):
    variancex_sum = 0.0
    variancey_sum = 0.0
    variancez_sum = 0.0
    with open(variances_file, "r") as inFile:
        for line in inFile:
            if line.startswith("variancex:"):
                variancex_sum += float(line.split(":")[1].strip())
            elif line.startswith("variancey:"):
                variancey_sum += float(line.split(":")[1].strip())
            elif line.startswith("variancez:"):
                variancez_sum += float(line.split(":")[1].strip())
    variance_sum = variancex_sum + variancey_sum + 100.*variancez_sum
    loss = variance_sum
    print("Summary variance:", variance_sum)
    with open(out_file, "a") as outFile:
        outFile.write(parameters_tochange["RecoBaseNameOut"]+": "+str(loss)+"\n")

    return loss

def lossfunction_direct(VarX, VarY, VarZ, parameters_tochange,out_file):
    variancex_sum = VarX
    variancey_sum = VarY
    variancez_sum = VarZ

    variance_sum = variancex_sum + variancey_sum + 100.*variancez_sum
    loss = variance_sum
    print("Summary variance:", variance_sum)
    with open(out_file, "a") as outFile:
        outFile.write(parameters_tochange["RecoBaseNameOut"]+": "+str(loss)+"\n")

    return loss



def eachiteration(dict_params):

    #dict_params["GeoNameOut"]=args.geoname

    config_in = "/home/yiming/Simu/build/config/"+dict_params["Config_Template"] # args.templateconf
    config_out = "/home/yiming/Simu/build/config/configG4_"+dict_params["RecoBaseNameOut"]+".txt"
    
    print("config :"+config_in+" "+config_out)
    
    modify_config_geant4(config_in,config_out,dict_params)
    max_wait_time = 6000 # Maximum waitting time, unit is second
    start_time = time.time()
    path1 = "/home/yiming/Simu/build/config/configG4_"+dict_params["RecoBaseNameOut"]+".txt"
    while not os.path.exists(path1):
        if time.time() - start_time > max_wait_time:
            raise TimeoutError("Timeout: File %s is not generated." % path1)
        print("No file: "+path1)
        time.sleep(3)
    
    run_geant4(config_out)
    start_time = time.time()
    path2 = "/home/yiming/Simu/build/geo_file/"+dict_params["GeoNameOut"]
    while not os.path.exists(path2):
        if time.time() - start_time > max_wait_time:
            raise TimeoutError("Timeout: File %s is not generated." % path2)
        print("No file: "+path2)
        time.sleep(3)
    
    run_reco(dict_params)
    for i in range(72):
        start_time = time.time()
        path3 = "/home/yiming/FullReco/output/"+dict_params["RecoBaseNameOut"]+"_%03d_finish" % i
        while not os.path.exists(path3):
            if time.time() - start_time > max_wait_time:
                raise TimeoutError("Timeout: File %s is not generated." % path3)
            print("No file: "+path3)
            time.sleep(10)
    
    run_anaCal(dict_params)
    for i in range(72):
        start_time = time.time()
        path4 = "/home/yiming/FullReco/draw/Hist/Hist_"+dict_params["RecoBaseNameOut"]+"_%03d_finish" % i
        while not os.path.exists(path4):
            if time.time() - start_time > max_wait_time:
                raise TimeoutError("Timeout: File %s is not generated." % path4)
            print("No file: "+path4)
            time.sleep(3)
    
    run_addHist(dict_params)
    start_time = time.time()
    path5 = "/home/yiming/FullReco/draw/Scan2/"+dict_params["RecoBaseNameOut"]+"_720000.root"
    while not os.path.exists(path5):
        if time.time() - start_time > max_wait_time:
            raise TimeoutError("Timeout: File %s is not generated." % path5)
        print("No file: "+path5)
        time.sleep(3)
    
    tempVarX, tempVarY, tempVarZ = run_createVar(dict_params)
    #variances_path = "/home/yiming/FullReco/draw/variances.txt"
    lossfile_path = "/home/yiming/FullReco/systematic_cal/lossfile3.txt"
    #loss = lossfunction(dict_params,variances_path,lossfile_path,loss)
    loss = lossfunction_direct(tempVarX,tempVarY,tempVarZ,dict_params,lossfile_path)

    return -loss # loss is decreasing to coverge from big to 0 -> the optimization is from -infinite to 0 so multiple by -1

def DetOptim(para):

    dict_params = {}
    dict_params["WASA_Calib_X"]=     para["WASA_Calib_X_Values"]
    dict_params["WASA_Calib_Y"]=     para["WASA_Calib_Y_Values"]
    dict_params["WASA_Calib_Z"]=     para["WASA_Calib_Z_Values"]
    dict_params["WASA_Calib_Rot_X"]= para["WASA_Calib_Rot_X_Values"]
    dict_params["WASA_Calib_Rot_Y"]= para["WASA_Calib_Rot_Y_Values"]
    dict_params["WASA_Calib_Rot_Z"]= para["WASA_Calib_Rot_Z_Values"]
    dict_params["MFT_Calib_X"]=      para["MFT_Calib_X_Values"]
    dict_params["MFT_Calib_Y"]=      para["MFT_Calib_Y_Values"]
    dict_params["MFT_Calib_Z"]=      para["MFT_Calib_Z_Values"]
    dict_params["MFT_Calib_Rot_X"]=  para["MFT_Calib_Rot_X_Values"]
    dict_params["MFT_Calib_Rot_Y"]=  para["MFT_Calib_Rot_Y_Values"]
    dict_params["MFT_Calib_Rot_Z"]=  para["MFT_Calib_Rot_Z_Values"]
    dict_params["GeoNameOut"]=para.pass_through["args.geoname"]
    #dict_params["RecoBaseNameOut"]=para.pass_through["args.basename"]

    #dict_params["RecoBaseNameOut"]="WASA_X"+dict_params["WASA_Calib_X"]+"Y"+dict_params["WASA_Calib_Y"]+"Z"+dict_params["WASA_Calib_Z"]+"RX"+dict_params["WASA_Calib_Rot_X"]+"RY"+dict_params["WASA_Calib_Rot_Y"]+"RZ"+dict_params["WASA_Calib_Rot_Z"]+"_MFT_X"+dict_params["MFT_Calib_X"]+"Y"+dict_params["MFT_Calib_Y"]+"Z"+dict_params["MFT_Calib_Z"]+"RX"+dict_params["MFT_Calib_Rot_X"]+"RY"+dict_params["MFT_Calib_Rot_Y"]+"RZ"+dict_params["MFT_Calib_Rot_Z"]
    dict_params["RecoBaseNameOut"]="WASA_X"+str(dict_params["WASA_Calib_X"])+"Y"+str(dict_params["WASA_Calib_Y"])+"Z"+str(dict_params["WASA_Calib_Z"])+"RX"+str(dict_params["WASA_Calib_Rot_X"])+"RY"+str(dict_params["WASA_Calib_Rot_Y"])+"RZ"+str(dict_params["WASA_Calib_Rot_Z"])+"MFT_X"+str(dict_params["MFT_Calib_X"])+"Y"+str(dict_params["MFT_Calib_Y"])+"Z"+str(dict_params["MFT_Calib_Z"])+"RX"+str(dict_params["MFT_Calib_Rot_X"])+"RY"+str(dict_params["MFT_Calib_Rot_Y"])+"RZ"+str(dict_params["MFT_Calib_Rot_Z"])

    dict_params["Config_Template"] = para.pass_through["args.templateconf"]

    print("dict params :",dict_params)

    return eachiteration(dict_params)

def main():
    parser = argparse.ArgumentParser(description='Systematic calibration')

    parser.add_argument('--params', help='the parameters of calibrations 12 of them', type=str, nargs=12, default =[0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.])
    parser.add_argument('--geoname',help='geometry output file from geant4',type=str,default='geotest.root')
#parser.add_argument('--basename',help='root base name for the output of the reco',type=str,default='GNN_defaultbasename_Reco')
    parser.add_argument('--templateconf',help='the template config file for geant4',type=str,default='')
    
    args = parser.parse_args()
    #dict_params = {}
    num=11

    #WASA_Calib_X_Values=np.linspace(-float(args.params[0]),float(args.params[0]),num)
    #WASA_Calib_Y_Values=np.linspace(-float(args.params[1]),float(args.params[1]),num)
    #WASA_Calib_Z_Values=np.linspace(-float(args.params[2]),float(args.params[2]),num)
    #WASA_Calib_Rot_X_Values=np.linspace(-float(args.params[3]),float(args.params[3]),num)
    #WASA_Calib_Rot_Y_Values=np.linspace(-float(args.params[4]),float(args.params[4]),num)
    #WASA_Calib_Rot_Z_Values=np.linspace(-float(args.params[5]),float(args.params[5]),num)
    #MFT_Calib_X_Values=np.linspace(-float(args.params[6]),float(args.params[6]),num)
    #MFT_Calib_Y_Values=np.linspace(-float(args.params[7]),float(args.params[7]),num)
    #MFT_Calib_Z_Values=np.linspace(-float(args.params[8]),float(args.params[8]),num)
    #MFT_Calib_Rot_X_Values=np.linspace(-float(args.params[9]),float(args.params[9]),num)
    #MFT_Calib_Rot_Y_Values=np.linspace(-float(args.params[10]),float(args.params[10]),num)
    #MFT_Calib_Rot_Z_Values=np.linspace(-float(args.params[11]),float(args.params[11]),num)
    WASA_Calib_X_Values=np.linspace(0.,float(args.params[0]),num)
    WASA_Calib_Y_Values=np.linspace(0.,float(args.params[1]),num)
    WASA_Calib_Z_Values=np.linspace(0.,float(args.params[2]),num)
    WASA_Calib_Rot_X_Values=np.linspace(0.,float(args.params[3]),num)
    WASA_Calib_Rot_Y_Values=np.linspace(0.,float(args.params[4]),num)
    WASA_Calib_Rot_Z_Values=np.linspace(0.,float(args.params[5]),num)
    MFT_Calib_X_Values=np.linspace(0.,float(args.params[6]),num)
    MFT_Calib_Y_Values=np.linspace(0.,float(args.params[7]),num)
    MFT_Calib_Z_Values=np.linspace(0.,float(args.params[8]),num)
    MFT_Calib_Rot_X_Values=np.linspace(0.,float(args.params[9]),num)
    MFT_Calib_Rot_Y_Values=np.linspace(0.,float(args.params[10]),num)
    MFT_Calib_Rot_Z_Values=np.linspace(0.,float(args.params[11]),num)

    search_space = {
        "WASA_Calib_X_Values":      WASA_Calib_X_Values.tolist(),
        "WASA_Calib_Y_Values":      WASA_Calib_Y_Values.tolist(),
        "WASA_Calib_Z_Values":      WASA_Calib_Z_Values.tolist(),
        "WASA_Calib_Rot_X_Values":  WASA_Calib_Rot_X_Values.tolist(),
        "WASA_Calib_Rot_Y_Values":  WASA_Calib_Rot_Y_Values.tolist(),
        "WASA_Calib_Rot_Z_Values":  WASA_Calib_Rot_Z_Values.tolist(),
        "MFT_Calib_X_Values":       MFT_Calib_X_Values.tolist(),
        "MFT_Calib_Y_Values":       MFT_Calib_Y_Values.tolist(),
        "MFT_Calib_Z_Values":       MFT_Calib_Z_Values.tolist(),
        "MFT_Calib_Rot_X_Values":   MFT_Calib_Rot_X_Values.tolist(),
        "MFT_Calib_Rot_Y_Values":   MFT_Calib_Rot_Y_Values.tolist(),
        "MFT_Calib_Rot_Z_Values":   MFT_Calib_Rot_Z_Values.tolist(),
    }

    pass_through_in = {
        "args.geoname":args.geoname,
        "args.templateconf":args.templateconf
    }

    #optimizer =  RandomAnnealingOptimizer()
    #optimizer = RepulsingHillClimbingOptimizer()
    optimizer = HillClimbingOptimizer()
    #optimizer =  RandomRestartHillClimbingOptimizer()
    #optimizer = ParallelTemperingOptimizer()
    #optimizer =  SimulatedAnnealingOptimizer()
    hyper = Hyperactive(verbosity = ["progress_bar", "print_results"],distribution = "multiprocessing",n_processes = "auto")
    hyper.add_search(DetOptim, search_space, n_iter=1000,#n_jobs=4,
                     optimizer=optimizer, pass_through=pass_through_in,
                     early_stopping={"n_iter_no_change":5000, "tol_abs":0.01})
    #                 progress_board=progress_board)
    hyper.run()

    search_data = hyper.results(DetOptim)

    print(hyper.best_para(DetOptim))
    print(hyper.best_score(DetOptim))

    score_max = np.amax(search_data["score"][search_data["score"]>0.])
    score_std = search_data["score"][search_data["score"]>0.].std()
    search_data_f = search_data[abs(search_data["score"]-score_max) < score_std]

    search_data_f = search_data_f.drop_duplicates(subset=["WASA_Calib_X_Values",
                                                          "WASA_Calib_Y_Values",
                                                          "WASA_Calib_Z_Values",
                                                          "WASA_Calib_Rot_X_Values",
                                                          "WASA_Calib_Rot_Y_Values",
                                                          "WASA_Calib_Rot_Z_Values",
                                                          "MFT_Calib_X_Values",
                                                          "MFT_Calib_Y_Values",
                                                          "MFT_Calib_Z_Values",
                                                          "MFT_Calib_Rot_X_Values",
                                                          "MFT_Calib_Rot_Y_Values",
                                                          "MFT_Calib_Rot_Z_Values",

                                                          ])
    print(search_data_f.sort_values("score",ascending=False))
    #print(np.array(search_data["score"][search_data["score"]>0.]))

    #parameter_names = list(search_data_f.keys())
    #fig1 = px.parallel_categories(search_data,color="score",color_continuous_scale=color_scale,dimensions=parameter_names,)
    #fig1.update_layout(width=950, height=700)
    #fig1.show()
    #fig.write_html("parallel_categories_plot.html")



if __name__ == '__main__':
    main()
