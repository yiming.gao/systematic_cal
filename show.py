import matplotlib.pyplot as plt

minv = 99999.
conf = ""
# 读取文件
with open("lossfile3.txt", "r") as file:
    lines = file.readlines()

# 提取数据
    y_values = []
    for line in lines:
        parts = line.split(":")
        if len(parts) >= 2:
            value = float(parts[1].strip())
            y_values.append(value)
            if value < minv:
                minv = value
                conf = parts[0]

# 画折线图
    x_values = range(1, len(y_values) + 1)  # 横坐标为行数
    plt.plot(x_values, y_values, marker='o', linestyle='-')

# 添加标题和标签
    plt.title('Line Plot')
    plt.xlabel('Row')
    plt.ylabel('Value')

    print("Config: ",conf," min value: ",minv)
# 显示图形
    plt.show()

